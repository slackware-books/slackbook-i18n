# Переклади книги «Основи Slackware Linux» (Slackware Linux Essentials, або SlackBook)

Пропонована структура проекту:

    <root>/
      en_US/
        slackbook/
          <OmegaT project>
      uk_UA/
        slackbook/
          <OmegaT project>
      [...]

Для кожної мови необхідний окремий проект OmegaT, тож для кожної мови структура підкаталогу буде відповідати структурі проекту OmegaT, наприклад:

    [...]
    en_US/
      slackbook/
        dictionary/*
        glossary/*
        omegat/*
        source/*
        target/*
        tm/*
        omegat.project

## Додаткова інформація

* [Керування командним проектом OmegaT][100]

[100]: https://wiki.documentfoundation.org/Documentation/Development/UsingOmegaT#Manage_a_Team_Project
