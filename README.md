## Slackware Linux Essentials (SlackBook) — український переклад

Сховище, призначене для перекладу книги «Основи Slackware Linux» (Slackware 
Linux Essentials, або SlackBook) з англійської мови за допомогою [OmegaT][1].

### Докладніше про цю діяльність:

* [Переклад книг][2] (TODO)
* [Поточний стан перекладу][3] (TODO)
* [Група обговорення][4] (приклад з LibreOffice Guides)

Ми будемо раді за будь-яку допомогу, не тільки при перекладі — перекладений 
текст необхідно перевірити, а також створити зображення (знятки з українським
інтерфейсом тощо).

[1]: https://omegat.org/
[2]: #TODO
[3]: #TODO
[4]: https://groups.google.com/forum/#!forum/sk-libreoffice-guides#EXAMPLE
