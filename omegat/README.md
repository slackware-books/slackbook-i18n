From the OmegaT User’s Guide, Appendix C, OmegaT Team Projects:

> The administrator may decide to include following folders and their contents
as well: `tm`, `glossary` and `dictionary`. Also `ignored_words.txt` and
`learned_words.txt` in the `omegat` (this) folder may be worth sharing and
maintaining on the team level. ***Avoid in any case adding `bak` files,
`project_stats.txt` and `project_stats_match.txt`, in the `omegat` subfolder,
as they would without any need or profit just bloat the repository***. You
might want to apply the same to the `target` folder and its contents.
